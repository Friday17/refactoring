//было 124 стало 114
window.onload = function(){
    
    var number; //Введенное число
    var OriginalSS; //Исходная система счисления
    
	//При изменении значения формы вызывается функция
    document.getElementById("OrNum1").onchange = TranslationOfNumbers;
    document.getElementById("OrNum2").onchange = TranslationOfNumbers;
    document.getElementById("OrNum3").onchange = TranslationOfNumbers;
    document.getElementById("OrNum4").onchange = TranslationOfNumbers;
    
    //Очистка формы
    document.getElementById("input").onclick = cleaning;
	
    function input(id, value){
        if(document.getElementById(id).value){
            window.number = document.getElementById(id).value;
            window.OriginalSS = value;
        }
    }
    //Получение числа и исх. СС
    function GetNumber(){
        input("OrNum1", 2);
        input("OrNum2", 8);
        input("OrNum3", 10);
        input("OrNum4", 16);
        
        return 0;
    }
	
    //Перевод числа в различные СС
    function TranslationOfNumbers(){
        GetNumber();
        var NewNumber = parseInt(window.number, window.OriginalSS);
        var valida=validation(window.number, window.OriginalSS);
        
        //Проверка корректности ввода
        if(valida){
            //Выполнение перевода
            switch(window.OriginalSS){
                //Если исходная СС 2-я
                case 2:
                    document.form.eight.value=(NewNumber.toString(8));
                    document.form.ten.value=(NewNumber.toString(10));
                    document.form.hex.value=(NewNumber.toString(16).toUpperCase());
                    break;
                    
                //Если исходная СС 8-я
                case 8:
                    document.form.two.value=(NewNumber.toString(2));
                    document.form.ten.value=(NewNumber.toString(10));
                    document.form.hex.value=(NewNumber.toString(16).toUpperCase());
                    break;
                    
                //Если исходная СС 10-я
                case 10:
                    document.form.two.value=(NewNumber.toString(2));
                    document.form.eight.value=(NewNumber.toString(8));
                    document.form.hex.value=(NewNumber.toString(16).toUpperCase());
                    break;
                    
                //Если исходная СС 16-я    
                case 16:
                    document.form.two.value=(NewNumber.toString(2));
                    document.form.eight.value=(NewNumber.toString(8));
                    document.form.ten.value=(NewNumber.toString(10));
                    break;
            }
        }
        else{
            cleaning();
        }
    }
     
    //Функция проверки корректности ввода
    function validation(number, OriginalSS){
        switch(OriginalSS){
            case 2:
                if( /[^01.-]/.test(number) ){
                    alert("Двоичное число может содержать только цифры 0 и 1");
                    return false;
                }
                
            case 8:
                if(/[^0-7.-]/.test(number)){
                   alert("Восьмеричное число может содержать только цифры 0 - 7");
                    return false;
                }
                
            case 10:
                if(/[^0-9.-]/.test(number)){
                    alert("Десятичное число может содержать только цифры 0 - 9");
                    return false;
                }
                
            case 16:
                if(/[^0-9a-fA-f.-]/.test(number)){
                    alert(" Шестнадцатеричное число может содержать только цифры 0-9, буквы A,B,C,D,E,F");
                    return false;
                }
            default:
                return true;
        }
    }
	
    //Очистка формы
    function cleaning(){
        document.forms["form"].reset();
    }
}